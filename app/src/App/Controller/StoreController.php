<?php

namespace App\Controller;

use App\Document\RetailersWithStores;
use App\Document\Store;
use Doctrine\ODM\MongoDB\DocumentManager;
use GeoJson\Geometry\Point;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StoreController extends Controller
{
    /** @var DocumentManager $dm */
    private $dm;

    public function __construct( DocumentManager $dm ) {
        $this->dm = $dm;
    }

    /**
     * @Route("/simple-stores", name="simple_stores")
     */
    public function storesNearbyAction(Request $request )
    {
        $point = new Point([ (float)$request->get('lon'), (float)$request->get('lat') ]);
        $radius = (int) $request->get('radius', 2000);

        $stores = $this->dm->getRepository(Store::class)->findStoresNearby($point, $radius);

        $results = array_map(function(Store $store) {
            return [
                'id' => $store->getId(),
                'retailer' => $store->getRetailer()->getId(),
                'address' => $store->getAddress(),
                'coordinates' => $store->getCoordinates()->getPoint(),
                'description' => $store->getDescription()
            ];
        }, $stores);

        return $this->json($results);

    }

    /**
     * @Route("/stores", name="store")
     */
    public function nearbyAction(Request $request )
    {
        $point = new Point([ (float)$request->get('lon'), (float)$request->get('lat') ]);
        $radius = (int) $request->get('radius', 2000);

        $storesAndRetailers = $this->dm->getRepository(Store::class)->findStoresAndRetailersNearby($point, $radius);
        $results = array_map(function(RetailersWithStores $rws) {

            $stores = $rws->getStores()->map(function(Store $store) {
                return [
                    'id' => $store->getId(),
                    'retailer' => $store->getRetailer()->getId(),
                    'address' => $store->getAddress(),
                    'coordinates' => $store->getCoordinates()->getPoint(),
                    'description' => $store->getDescription()
                ];
            });
            $retailer = [
                'id' => $rws->getRetailer()->getId(),
                'name' => $rws->getRetailer()->getName(),
                'assets' => $rws->getRetailer()->getAssets()
            ];
            return [
                'retailer' => $retailer,
                'stores'   => $stores
            ];
        }, $storesAndRetailers);

        return $this->json($results);

    }


}
