<?php

namespace App\Controller;

use App\Document\Offer;
use App\Document\Product;
use App\Document\ProductGroup;
use App\Document\ProductGroupRef;
use Carbon\Carbon;
use Doctrine\ODM\MongoDB\DocumentManager;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OfferController extends Controller {
    /** @var DocumentManager $dm */
    private $dm;

    /** @var LoggerInterface */
    private $logger;

    public function __construct( DocumentManager $dm, LoggerInterface $logger) {
        $this->dm = $dm;
        $this->logger = $logger;
    }

    /**
     * @Route("/calendar", name="calendar")
     * @Template()
     */
    public function calendar(Request $request) {

        $date = $request->get('date');
        if ($date) {
            $month = Carbon::createFromFormat('Y-m-d', $date)->startOfMonth()->setTime(0,0,0);
        } else {
            $month = Carbon::create(2013,1,1)->startOfDay()->setTime(0,0,0);
        }

        $repo = $this->dm->getRepository(Product::class);
        $calendar = $repo->aggregateOfferCalendar($month);

        return [
            'month' => $month,
            'calendar' => $calendar
        ];
    }

    /**
     * @Route("/offers", name="offers")
     */
    public function offersOnDayAction( Request $request ) {

        $criteria = [];

        $date = $request->get('date');
        if ($date) {
            $date = Carbon::createFromFormat('Y-m-d', $date)->setTime(0,0,0);
            $criteria['date'] = $date;
        }

        $retailerId = $request->get('retailer');
        if ($retailerId) {
            $criteria['retailer'] = $retailerId;
        }

        $group = $request->get('group');
        if ($group) {
            $criteria['group'] = $group;
        }

        $repo = $this->dm->getRepository(Product::class);

        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 25);
        if (count($criteria) == 3) {
            $products = $repo->findByRetailerDateAndGroup($retailerId, $date, $group);
        } else {
            $products = $repo->findOffersFor($criteria, $offset, $limit);
        }

        $res = [];
        foreach ($products as $product) {
            /** @var Product $product */

            if ($date) {
                $offer = $product->getOffers()->filter(function(Offer $o) use ($criteria, $date) {
                    return $o->getStartedAt() <= $date && $o->getEndedAt() >= $date;
                })->first();
            } else {
                $offer = $product->getOffers()->first();
            }

            if (!$offer) {
                $this->logger->warn("no offer on product {$product->getId()}");
                continue;
            }

            $res['products'][] = [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'short_text' => $product->getShortText(),
                'offer' => [
                        'price' => $offer->getPrice(),
                        'avail' => [ 'started_at' => $offer->getStartedAt(), 'ended_at' => $offer->getEndedAt() ],
                        'retailer' => $offer->getRetailer()->getId(),
                        'link' => $offer->getLink()
                ],
                'groups' => array_map(function(ProductGroupRef $ref) {
                    return [
                        'id' => $ref->getGroup()->getId(),
                        'name' => $ref->getGroup()->getName(),
                        'paths' => $ref->getPaths()
                    ];
                }, $product->getGroups()->toArray()),
                'assets' => $product->getAssets(),
                'retailer' => $product->getRetailer()->getName()
            ];
        }
        return $this->json($res);
    }


}
