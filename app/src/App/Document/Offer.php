<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument
 * @MongoDB\Indexes({
 *   @MongoDB\Index(keys={"startedAt"="asc", "endedAt"="asc"})
 * })
 */
class Offer {

    /**
     * @var int
     * @MongoDB\Field(type="int")
     */
    private $oldId;

    /**
     * @var Retailer
     * @MongoDB\ReferenceOne(targetDocument="Retailer", storeAs="dbRef")
     */
    private $retailer;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $link;

    /**
     * @var int
     * @MongoDB\Field(type="int")
     */
    private $price;

    /**
     * @var int
     * @MongoDB\Field(type="int")
     */
    private $oldPrice;

    /**
     * @var int
     * @MongoDB\Field(type="int")
     */
    private $shippingCosts;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    private $startedAt;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    private $endedAt;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $customId;

    /**
     * @return int
     */
    public function getOldId(): int {
        return $this->oldId;
    }

    /**
     * @param int $oldId
     */
    public function setOldId( int $oldId ): void {
        $this->oldId = $oldId;
    }

    /**
     * @return Retailer
     */
    public function getRetailer(): Retailer {
        return $this->retailer;
    }

    /**
     * @param Retailer $retailer
     */
    public function setRetailer( Retailer $retailer ): void {
        $this->retailer = $retailer;
    }

    /**
     * @return string
     */
    public function getLink(): string {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink( ?string $link ): void {
        $this->link = $link;
    }

    /**
     * @return int
     */
    public function getPrice(): int {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice( int $price ): void {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getOldPrice(): ?int {
        return $this->oldPrice;
    }

    /**
     * @param int $oldPrice
     */
    public function setOldPrice( ?int $oldPrice ): void {
        $this->oldPrice = $oldPrice;
    }

    /**
     * @return int
     */
    public function getShippingCosts(): ?int {
        return $this->shippingCosts;
    }

    /**
     * @param int $shippingCosts
     */
    public function setShippingCosts( ?int $shippingCosts ): void {
        $this->shippingCosts = $shippingCosts;
    }

    /**
     * @return \DateTime
     */
    public function getStartedAt(): \DateTime {
        return $this->startedAt;
    }

    /**
     * @param \DateTime $startedAt
     */
    public function setStartedAt( \DateTime $startedAt ): void {
        $this->startedAt = $startedAt;
    }

    /**
     * @return \DateTime
     */
    public function getEndedAt(): \DateTime {
        return $this->endedAt;
    }

    /**
     * @param \DateTime $endedAt
     */
    public function setEndedAt( \DateTime $endedAt ): void {
        $this->endedAt = $endedAt;
    }

    /**
     * @return string
     */
    public function getCustomId(): string {
        return $this->customId;
    }

    /**
     * @param string $customId
     */
    public function setCustomId( ?string $customId ): void {
        $this->customId = $customId;
    }



}