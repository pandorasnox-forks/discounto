<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Vendor {

    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string $name
     */
    protected $name;

    /**
     * @MongoDB\Field(type="string")
     * @var string $logo
     */
    protected $logo;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId( $id ): void {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName( string $name ): void {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLogo(): string {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo( string $logo ): void {
        $this->logo = $logo;
    }



}