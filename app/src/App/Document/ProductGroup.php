<?php


namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedOne;

/**
 * @MongoDB\Document(collection="product_group")
 *   @MongoDB\Indexes({
 *      @MongoDB\Index(keys={"oldId"="asc"}),
 *      @MongoDB\Index(keys={"paths"="asc"})
 * })
 */
class ProductGroup {

    /**
     * @var string
     * @MongoDB\Id(strategy="NONE", type="string")
     */
    private $id;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $name;

    /**
     * @var $string
     * @MongoDB\Field(type="string")
     */
    private $slug;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $shortText;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $longText;

    /**
     * @var Meta
     * @MongoDB\EmbedOne(targetDocument="App\Document\Meta")
     */
    private $meta;

    /**
     * @var int
     * @MongoDB\Field(type="integer")
     */
    private $oldId;

    /**
     * @var array
     * @MongoDB\Field(type="collection")
     */
    private $paths;

    public function __construct() {
        $this->paths = [];
    }

    /**
     * @return string
     */
    public function getId(): string {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId( string $id ): void {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOldId(): int {
        return $this->oldId;
    }

    /**
     * @param int $oldId
     */
    public function setOldId( int $oldId ): void {
        $this->oldId = $oldId;
    }


    /**
     * @return string
     */
    public function getShortText(): string {
        return $this->shortText;
    }

    /**
     * @param string $shortText
     */
    public function setShortText( string $shortText ): void {
        $this->shortText = $shortText;
    }

    /**
     * @return string
     */
    public function getLongText(): string {
        return $this->longText;
    }

    /**
     * @param string $longText
     */
    public function setLongText( string $longText ): void {
        $this->longText = $longText;
    }

    /**
     * @return Meta
     */
    public function getMeta(): Meta {
        return $this->meta;
    }

    /**
     * @param Meta $meta
     */
    public function setMeta( Meta $meta ): void {
        $this->meta = $meta;
    }

    /**
     * @return array
     */
    public function getPaths(): array {
        return $this->paths;
    }

    /**
     * @param array $paths
     */
    public function setPaths( array $paths ): void {
        $this->paths = $paths;
    }

    public function addPath( string $path ): void {
        $this->paths[] = $path;
    }
    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName( string $name ): void {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug( $slug ): void {
        $this->slug = $slug;
    }


}