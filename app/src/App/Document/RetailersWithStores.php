<?php

namespace App\Document;

use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @MongoDB\QueryResultDocument
 */
class RetailersWithStores {

    /**
     * @var Retailer
     * @MongoDB\ReferenceOne(targetDocument="Retailer", name="_id")
     */
    private $retailer;

    /**
     * @var Collection
     * @MongoDB\EmbedMany(targetDocument="Store", name="stores" )
     */
    private $stores;

    /**
     * @return Retailer
     */
    public function getRetailer(): Retailer
    {
        return $this->retailer;
    }

    /**
     * @param Retailer $retailer
     */
    public function setRetailer(Retailer $retailer): void
    {
        $this->retailer = $retailer;
    }

    /**
     * @return Collection
     */
    public function getStores(): Collection
    {
        return $this->stores;
    }

    /**
     * @param Collection $stores
     */
    public function setStores(Collection $stores): void
    {
        $this->stores = $stores;
    }



}