<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/** @MongoDB\EmbeddedDocument */
class Coordinates {

    /**
     * @var float
     * @MongoDB\Field(type="float", name="x")

     **/
    protected $longitude;

    /**
     * @var float
     * @MongoDB\Field(type="float", name="y")

     **/
    protected $latitude;

    /**
     * @return float
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    /**
     * @return float
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    public function getPoint() {
        return [
            'lat' => $this->getLatitude(),
            'lon' => $this->getLongitude()
        ];
    }

    public function isNull() {
        return (($this->getLatitude() == 0) || ($this->getLongitude() == 0));
    }
    /**
     * @return string comma separated
     */
    public function getCommaSeparated() {
        return "{$this->latitude},{$this->longitude}";
    }
}