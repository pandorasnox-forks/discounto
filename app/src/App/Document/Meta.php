<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument()
 */
class Meta {

    const ACTIVE        = "aktiv";
    const CLOSED        = "closed";
    const DELETED       = "deleted";
    const STANDBY       = "standby";

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    private $modifiedAt;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    private $deletedAt;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $status;

    /**
     * Meta constructor.
     */
    public function __construct() {
        $this->status = self::ACTIVE;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt( ?\DateTime $createdAt ): void {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt(): ?\DateTime {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt( ?\DateTime $modifiedAt ): void {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt(): ?\DateTime {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt( ?\DateTime $deletedAt ): void {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus( string $status ): void {
        $this->status = $status;
    }


}