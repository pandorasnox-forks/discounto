<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument()
 */
class Address {

    /**
     * @var int
     * @MongoDB\Field(type="int")
     */
    private $oldId;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $zip;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $city;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $citypart;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $street;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $streetnumber;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $area;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $area2;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $county;

    /**
     * @return int
     */
    public function getOldId(): int {
        return $this->oldId;
    }

    /**
     * @param int $oldId
     */
    public function setOldId( int $oldId ): void {
        $this->oldId = $oldId;
    }

    /**
     * @return string
     */
    public function getZip(): string {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip( ?string $zip ): void {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCity(): string {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity( ?string $city ): void {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCitypart(): string {
        return $this->citypart;
    }

    /**
     * @param string $citypart
     */
    public function setCitypart( ?string $citypart ): void {
        $this->citypart = $citypart;
    }

    /**
     * @return string
     */
    public function getStreet(): string {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet( ?string $street ): void {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getStreetnumber(): string {
        return $this->streetnumber;
    }

    /**
     * @param string $streetnumber
     */
    public function setStreetnumber( ?string $streetnumber ): void {
        $this->streetnumber = $streetnumber;
    }

    /**
     * @return string
     */
    public function getArea(): string {
        return $this->area;
    }

    /**
     * @param string $area
     */
    public function setArea( ?string $area ): void {
        $this->area = $area;
    }

    /**
     * @return string
     */
    public function getArea2(): string {
        return $this->area2;
    }

    /**
     * @param string $area2
     */
    public function setArea2( ?string $area2 ): void {
        $this->area2 = $area2;
    }

    /**
     * @return string
     */
    public function getCounty(): string {
        return $this->county;
    }

    /**
     * @param string $county
     */
    public function setCounty( ?string $county ): void {
        $this->county = $county;
    }


}