<?php


namespace App\Repository;


use App\Document\RetailersWithStores;
use Carbon\Carbon;
use Doctrine\ODM\MongoDB\DocumentRepository;
use GeoJson\Geometry\Point;

class StoreRepository extends DocumentRepository {

    public function findStoresNearby(Point $point, int $radius) {
        $qb = $this->createQueryBuilder();
        $qb->field('coordinates')
           ->nearSphere($point)
           ->minDistance(0)
           ->maxDistance($radius);
        $res = $qb->getQuery()->toArray();
        return $res;
    }

    public function findStoresAndRetailersNearby(Point $point, int $radius) : array {
        $ab = $this->createAggregationBuilder();
        $ab->hydrate(RetailersWithStores::class)
            ->geoNear($point)
                ->spherical(true)
                ->minDistance(0)
                ->maxDistance($radius)
                ->distanceField('distance')
                ->limit(500)
            ->group()
                ->field("_id")
                ->expression('$retailer')
                ->field("stores")
                ->push('$$ROOT')
                ;

        $arr = $ab->execute()->toArray();
        return $arr;
    }
}