<?php


namespace App\Repository;


use App\Document\Retailer;
use Carbon\Carbon;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\ODM\MongoDB\Query\Builder;
use Doctrine\ORM\QueryBuilder;

class ProductRepository extends DocumentRepository {

    public function aggregateOfferCalendar($start) {

        $end   = $start->copy()->addMonth();

        $ab = $this->createAggregationBuilder();
        $ab->unwind('$offers')
            ->match()->field("offers.startedAt")
                     ->gt($start)
                     ->lt($end)
            ->group()->field("_id")
                ->expression( $ab->expr()
                    ->field("day")
                    ->dayOfMonth('$offers.startedAt')
                    ->field("retailer")
                    ->expression('$offers.retailer')
                )
                ->field('daycount')
                ->sum(1)

            ->group()->field('_id')->expression('$_id.day')
                ->field('retailer')->push($ab->expr()
                        ->field('retailer')->expression('$_id.retailer')
                        ->field('count')->expression('$daycount')
                )
                ->field('count')->sum('$daycount')
            ->sort(["_id" => 1])
        ;

        $res = $ab->execute()->toArray();
        return $res;

    }

    public function findByRetailerDateAndGroup(string $retailerId, Carbon $date,string $group) {
        $qb = $this->createQueryBuilder();
        $start = $date->copy()->startOfDay();
        $end   = $date->copy()->endOfDay();
        $regex = new \MongoRegex("/,$group,/");

        $qb->field('offers.retailer.$id')->equals($retailerId)
           ->field( "offers" )->elemMatch(
               $qb->expr()
                   ->field( "startedAt" )->lte( $start )
                   ->field("endedAt")->gte( $end)
            )
            ->field("groups.paths")->equals($regex);
        return $qb->getQuery()->execute();
    }

    public function findOffersStartingOnDate(Carbon $date) {
        $qb = $this->createQueryBuilder();

        $start = $date->copy()->startOfDay();
        $end   = $date->copy()->endOfDay();

        $qb->field( "offers" )->elemMatch( $qb->expr()
                  ->field( "startedAt" )
                  ->gte( $start )
                  ->lte( $end)
        );

        return $qb->getQuery()->execute();
    }

    private function availableOnDate(Builder $qb, Carbon $date) : Builder {
        $start = $date->copy()->startOfDay();
        $end   = $date->copy()->endOfDay();

        $qb->field( "offers" )->elemMatch( $qb->expr()
              ->field( "startedAt" )
              ->lte( $start )
            ->field("endedAt")->gte( $end)
        );

        return $qb;
    }

    private function inGroup(Builder $qb, string $group) : Builder {

        $re = new \MongoRegex("/,$group,/");
        $qb->field("groups.paths")->equals($re);
        return $qb;
    }

    public function findOffersFor(array $criteria, $offset = 0, $limit = 25) {

        $qb = $this->createQueryBuilder();
        if (isset($criteria['retailer'])) {
            $retailerId = $criteria['retailer'] instanceof Retailer ?
                $criteria['retailer']->getId() :
                $criteria['retailer']
            ;
            $qb->field('offers.retailer.$id')->equals($retailerId);
        }

        if (isset($criteria['date'])) {
            $date = $criteria['date'] instanceof Carbon ? $criteria['date'] : Carbon::instance($criteria['date']);
            $qb = $this->availableOnDate($qb, $date);
        }

        if (isset($criteria['group'])) {
            $qb = $this->inGroup($qb,$criteria['group']);
        }

        $qb->skip($offset)->limit($limit);

        return $qb->getQuery()->execute();
    }
}