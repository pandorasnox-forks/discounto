import React, {Children, Component} from 'react'
import Map from './Map';
import Offers from './Offers';
const { DateTime } = require('luxon');
import DayPickerInput from 'react-day-picker/DayPickerInput';

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            retailer: undefined,
            selectedDay: DateTime.fromISO("2013-01-11")
        };
        this.handleDayChange = this.handleDayChange.bind(this);
    }

    handleDayChange(selectedDay) {
        this.setState({ selectedDay: DateTime.fromJSDate(selectedDay) })
    }


    render() {
        return <div>

            <section className="section">
                <div className="container">
                    <h1 className="title">
                        Offers
                    </h1>
                </div>
            </section>

            <DayPickerInput onDayChange={this.handleDayChange} inputProps={{className: "input"}} value="2013-01-11"/>

            <Map lat={52.492142} lon={13.423908} onRetailerSelected={retailer => this.setState({retailer})} />

            <Offers retailer={this.state.retailer} day={this.state.selectedDay}/>

            <div>



            </div>

        </div>
    }
}
