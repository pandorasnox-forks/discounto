```
docker-compose up
```

nginx listens on localhost:8090  
mongodb is exposed on your machine as 27018

login to the php container and 
```
composer install
```

The frontend app is powered by React. Sincy I love to run node on my own machine, you got to create assets locally (or extend my Dockerfile and send me an PR ;) ) :

```
npm i
encore dev
```


